%define NEXT_KEY 0
%macro colon 2

%ifid %2
    %ifdef NEXT_KEY
			%%previous: dq NEXT_KEY		; Локальная метка previous
		%else
			%%previous: dq 0
		%endif
		%define NEXT_KEY %%previous		; В POINTER всегда адрес начала списка
	%else
		%fatal 'Второй элемент не ID'
	%endif

%ifstr %1    
    db %1, 0
%else
		%fatal 'first element is not a string'
%endif

%define NEXT_KEY %2
%endmacro
